CREATE OR REPLACE PACKAGE BODY CS_HEADER_BILL_PKG AS

  PROCEDURE INSERT_HEADER_PR (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PISTBILL_NUMBER    IN VARCHAR2,
                              PIINCUSTOMER_ID    NUMBER
                             ) IS
  
  linIdHeader NUMBER;
  

  BEGIN
      SELECT HEADER_BILL_ID_SEQ.NEXTVAL INTO linIdHeader FROM DUAL; 
      
      BEGIN
          INSERT INTO HEADER_BILLS_TAB(HEADER_ID,
                                       BILL_NUMBER,
                                       CUSTOMER_ID,
                                       STATUS,
                                       CREATED_BY,
                                       CREATION_DATE,
                                       LAST_UPDATE_DATE)
                                VALUES(linIdHeader,
                                       PISTBILL_NUMBER,
                                       PIINCUSTOMER_ID,
                                       'OPEN',
                                       '1',
                                       SYSDATE,
                                       SYSDATE
                                      ) ;
                                   COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
      END;                                        
                                            
      POSTEXIT:= 'Registro Creado';
   
  END INSERT_HEADER_PR;
  
  PROCEDURE UPDATE_HEADER_PR (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PIINHEADER_ID      NUMBER,
                              PISTBILL_NUMBER    IN VARCHAR2,
                              PIINCUSTOMER_ID    IN VARCHAR2,
                              PISTSTATUS         IN VARCHAR2
                             )  AS
                               
                               
    lstBillNum      VARCHAR2(300);  
    linCust         NUMBER; 
    lstStatus        VARCHAR2(30); 
  
  BEGIN
    BEGIN
        SELECT BILL_NUMBER,
               CUSTOMER_ID,
               STATUS
        INTO  lstBillNum,
              linCust,
              lstStatus          
        FROM  HEADER_BILLS_TAB                             
        WHERE HEADER_ID = PIINHEADER_ID
        AND   STATUS NOT IN ('CLOSE','CANCEL');
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        POSTERROR:= ' No se encontro factura: '|| PIINHEADER_ID ;
    END;    
                                
    BEGIN
        UPDATE HEADER_BILLS_TAB SET BILL_NUMBER   = NVL(PISTBILL_NUMBER,lstBillNum),
                                    CUSTOMER_ID   = NVL(PIINCUSTOMER_ID,linCust),
                                    STATUS        = NVL(PISTSTATUS,lstStatus)                                 
        WHERE HEADER_ID = PIINHEADER_ID
        AND   STATUS NOT IN ('CLOSE','CANCEL');
        COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
    END;                                        
                                            
      POSTEXIT:= 'Registro Actualizado';    
  
  END UPDATE_HEADER_PR; 
  
  PROCEDURE DELETE_HEADER_PR (POSTERROR      OUT VARCHAR2,
                              POSTEXIT       OUT VARCHAR2,
                              PIINHEADER_ID     IN NUMBER
                              )AS
  BEGIN
      BEGIN
          DELETE  HEADER_BILLS_TAB
          WHERE   HEADER_ID = PIINHEADER_ID
          AND     STATUS NOT IN ('CLOSE','CANCEL');
          COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
          POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;      
      END;
      POSTEXIT:= 'Registro Borrado';
  END DELETE_HEADER_PR;                                
  
END CS_HEADER_BILL_PKG;  
/