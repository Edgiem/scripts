CREATE OR REPLACE PACKAGE CS_GET_BILL_PKG AS

  PROCEDURE GET_HEADER_PR(POSTERROR          OUT VARCHAR2,
                          POSTEXIT           OUT VARCHAR2, 
                          PIINIDHEADER       NUMBER,
                          PORCRESULT         OUT SYS_REFCURSOR);


  PROCEDURE GET_LINE_PR(POSTERROR          OUT VARCHAR2,
                        POSTEXIT           OUT VARCHAR2, 
                        PIINIDHEADER       NUMBER,
                        PORCRESULT         OUT SYS_REFCURSOR);

END CS_GET_BILL_PKG;
/