CREATE OR REPLACE PACKAGE CS_HEADER_BILL_PKG AS

  PROCEDURE INSERT_HEADER_PR (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PISTBILL_NUMBER    IN VARCHAR2,
                              PIINCUSTOMER_ID    NUMBER
                              );
                              
  PROCEDURE UPDATE_HEADER_PR (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PIINHEADER_ID      NUMBER,
                              PISTBILL_NUMBER    IN VARCHAR2,
                              PIINCUSTOMER_ID    IN VARCHAR2,
                              PISTSTATUS         IN VARCHAR2
                             );
  PROCEDURE DELETE_HEADER_PR (POSTERROR      OUT VARCHAR2,
                              POSTEXIT       OUT VARCHAR2,
                              PIINHEADER_ID     IN NUMBER
                              );    

END CS_HEADER_BILL_PKG;
							  