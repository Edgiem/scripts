CREATE OR REPLACE PACKAGE BODY CS_GET_BILL_PKG AS

  PROCEDURE GET_HEADER_PR(POSTERROR          OUT VARCHAR2,
                          POSTEXIT           OUT VARCHAR2, 
                          PIINIDHEADER       NUMBER,
                          PORCRESULT         OUT SYS_REFCURSOR) AS
  BEGIN
  
   BEGIN
	   OPEN porcResult FOR
		SELECT  H.BILL_NUMBER,
				CT.CUSTOMER_NAME,
				CT.CUSTOMER_CODE RFC,
				CT.ADRESS,
				H.STATUS,
                TRUNC(H.CREATION_DATE)		
		FROM    HEADER_BILLS_TAB H,
				CUSTOMER_TAB CT
		WHERE   CT.CUSTOMER_ID = H.CUSTOMER_ID
		AND     H.HEADER_ID    = PIINIDHEADER;
	EXCEPTION
        WHEN OTHERS THEN
            POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
	END;
	
	IF POSTERROR IS NULL THEN      
          POSTEXIT:= 'Registro Duevuelto';
    END IF;
    
  END GET_HEADER_PR;
  
  PROCEDURE GET_LINE_PR(POSTERROR          OUT VARCHAR2,
                        POSTEXIT           OUT VARCHAR2, 
                        PIINIDHEADER       NUMBER,
                        PORCRESULT         OUT SYS_REFCURSOR) AS 
  BEGIN
  
	  BEGIN
		  OPEN porcResult FOR
		  SELECT  IT.PRODUCT_NAME,
				  IT.DESCRIPTION,
				  IT.MEASURE,
				  IT.UNIT_PRICE,
				  L.AMOUNT,
				  L.AMOUNT_PRICE    
		  FROM    LINES_BILLS_TAB L,
				  INVENTORY_TAB IT
		  WHERE   IT.INVENTORY_ID = L.INVENTORY_ID
		  AND     L.HEADER_ID     = PIINIDHEADER;
		
	  
	  EXCEPTION
			WHEN OTHERS THEN
				POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
	  END;

    IF POSTERROR IS NULL THEN      
        POSTEXIT:= 'Registros Duevuelto';
    END IF;
	
  END GET_LINE_PR;  
  
END CS_GET_BILL_PKG;
/