CREATE OR REPLACE PACKAGE BODY CS_LINE_BILL_PKG AS

  PROCEDURE INSERT_LINES_PR (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PIINHEADER_ID      NUMBER,
                              PIININV_ID         NUMBER,
                              PIINAMOUNT         NUMBER    
                              ) IS
  
  linIdLine NUMBER;
  linPrecio NUMBER;
  

  BEGIN
      SELECT LINE_BILL_ID_SEQ.NEXTVAL INTO linIdLine FROM DUAL; 
      
      SELECT  UNIT_PRICE * PIINAMOUNT
      INTO    linPrecio 
      FROM    INVENTORY_TAB
      WHERE   INVENTORY_ID = PIININV_ID;
      
      BEGIN
          INSERT INTO LINES_BILLS_TAB (HEADER_ID,
                                       LINES_ID,
                                       INVENTORY_ID,
                                       AMOUNT,
                                       AMOUNT_PRICE,
                                       CREATED_BY,
                                       CREATION_DATE,
                                       LAST_UPDATE_DATE)
                                VALUES(PIINHEADER_ID,
                                       linIdLine,
                                       PIININV_ID,
                                       PIINAMOUNT,
                                       linPrecio,
                                       '1',
                                       SYSDATE,
                                       SYSDATE
                                      ) ;
                                   COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
      END;                                        
      
      IF POSTERROR IS NULL THEN      
          POSTEXIT:= 'Registro Creado';
      END IF;      
   
  END INSERT_LINES_PR;
  
  PROCEDURE UPDATE_LINES_PR  (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PIINLINE_ID        NUMBER,
                              PIINHEADER_ID      NUMBER,
                              PIININV_ID         NUMBER,
                              PIINAMOUNT         NUMBER
                             )  AS
                               
    linPrecio      NUMBER;                           
    linInvId       NUMBER;  
    linCantidad    NUMBER; 
    linCosto       NUMBER; 
  
  BEGIN
  
    IF PIINAMOUNT IS NOT NULL THEN
        SELECT  UNIT_PRICE * PIINAMOUNT
        INTO    linPrecio 
        FROM    INVENTORY_TAB
        WHERE   INVENTORY_ID = PIININV_ID;
    ELSE
       linPrecio := NULL;
    END IF;
    
    BEGIN
        SELECT INVENTORY_ID,
               AMOUNT,
               AMOUNT_PRICE
        INTO  linInvId,
              linCantidad,
              linCosto          
        FROM  LINES_BILLS_TAB                             
        WHERE LINES_ID = PIINLINE_ID
        AND   HEADER_ID    = PIINHEADER_ID;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        POSTERROR:= ' No se encontro Linea: '|| PIINHEADER_ID ;
    END;    
                                
    BEGIN
        UPDATE LINES_BILLS_TAB SET INVENTORY_ID  = NVL(PIININV_ID,linInvId),
                                    AMOUNT       = NVL(PIINAMOUNT,linCantidad),
                                    AMOUNT_PRICE = NVL(linPrecio,linCosto)                                 
        WHERE LINES_ID  = PIINLINE_ID
        AND   HEADER_ID = PIINHEADER_ID;
        COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
    END;                                        
                                            
      POSTEXIT:= 'Registro Actualizado';    
  
  END UPDATE_LINES_PR; 
  
  PROCEDURE DELETE_LINES_PR  (POSTERROR      OUT VARCHAR2,
                              POSTEXIT       OUT VARCHAR2,
                              PIINHEADER_ID  IN NUMBER,
                              PIINLINE_ID    IN NUMBER
                              )AS
  BEGIN
      BEGIN
          DELETE LINES_BILLS_TAB
          WHERE LINES_ID  = PIINLINE_ID
          AND   HEADER_ID = PIINHEADER_ID;
          COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
          POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;      
      END;
      POSTEXIT:= 'Registro Borrado';
  END DELETE_LINES_PR;                                
  
END CS_LINE_BILL_PKG;  
/