CREATE OR REPLACE PACKAGE BODY CS_CUSTOMER_PKG AS

PROCEDURE INSERT_CUSTOMER_PR (POSTERROR           OUT VARCHAR2,
                              POSTEXIT            OUT VARCHAR2,
                              PISTCUSTOMER_NAME   IN VARCHAR2,
                              PISTCUST_LAST_NAME  IN VARCHAR2,
                              PISTCUSTOMER_CODE   IN VARCHAR2,
                              PISTCUSTOMER_CAT    IN VARCHAR2,
                              PISTADRESS          IN VARCHAR2,
                              PIINPHONE_NUMBER    IN NUMBER,
                              PISTEMAIL           IN VARCHAR2
                              ) IS
  
 linIdCustomer NUMBER;
  

  BEGIN
      SELECT CUSTOMER_ID_SEQ.NEXTVAL INTO linIdCustomer FROM DUAL; 
      
      BEGIN
          INSERT INTO CUSTOMER_TAB(CUSTOMER_ID,
                                   CUSTOMER_NAME,
                                   CUST_LAST_NAME,
                                   CUSTOMER_CODE,
                                   CUSTOMER_CAT,
                                   ADRESS,
                                   PHONE_NUMBER,
                                   EMAIL,
                                   STATUS,
                                   CREATED_BY,
                                   CREATION_DATE,
                                   LAST_UPDATE_DATE)
                            VALUES(linIdCustomer,
                                   PISTCUSTOMER_NAME,
                                   PISTCUST_LAST_NAME,
                                   PISTCUSTOMER_CODE,
                                   PISTCUSTOMER_CAT,
                                   PISTADRESS,
                                   PIINPHONE_NUMBER,
                                   PISTEMAIL,
                                   'A',
                                   '1',
                                   SYSDATE,
                                   SYSDATE
                                  ) ;
                                  COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
      END;                                        
                                            
      POSTEXIT:= 'Registro Creado';
   
  END INSERT_CUSTOMER_PR;
  
  PROCEDURE UPDATE_CUSTOMER_PR (POSTERROR           OUT VARCHAR2,
                                POSTEXIT            OUT VARCHAR2,
                                PIINCUSTOMER_ID     IN NUMBER,
                                PISTCUSTOMER_NAME   IN VARCHAR2,
                                PISTCUST_LAST_NAME  IN VARCHAR2,
                                PISTCUSTOMER_CODE   IN VARCHAR2,
                                PISTCUSTOMER_CAT    IN VARCHAR2,
                                PISTADRESS          IN VARCHAR2,
                                PIINPHONE_NUMBER    IN NUMBER,
                                PISTEMAIL           IN VARCHAR2
                               ) AS
                               
                               
    lstName      VARCHAR2(300);  
    lstLastName  VARCHAR2(300);
    lstCode      VARCHAR2(30);
    lstCat       VARCHAR2(1);
    lstAdress    VARCHAR2(300);
    linPhone     NUMBER;
    lstEmail     VARCHAR2(30);                               
  
  BEGIN
    BEGIN
        SELECT CUSTOMER_NAME,
               CUST_LAST_NAME,
               CUSTOMER_CODE,
               CUSTOMER_CAT,
               ADRESS,
               PHONE_NUMBER,
               EMAIL
        INTO  lstName,
              lstLastName,
              lstCode,
              lstCat,
              lstAdress,
              linPhone,
              lstEmail              
        FROM  CUSTOMER_TAB                             
        WHERE CUSTOMER_ID = PIINCUSTOMER_ID;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        POSTERROR:= ' No se encontro cliente: '|| PIINCUSTOMER_ID ;
    END;    
                                
    BEGIN
        UPDATE CUSTOMER_TAB SET CUSTOMER_NAME    = NVL(PISTCUSTOMER_NAME,lstName),
                                CUST_LAST_NAME   = NVL(PISTCUST_LAST_NAME,lstLastName),
                                CUSTOMER_CODE    = NVL(PISTCUSTOMER_CODE,lstCode),
                                CUSTOMER_CAT     = NVL(PISTCUSTOMER_CAT,lstCat),
                                ADRESS           = NVL(PISTADRESS,lstAdress),
                                PHONE_NUMBER     = NVL(PIINPHONE_NUMBER,linPhone),
                                EMAIL            = NVL(PISTEMAIL,lstEmail),
                                LAST_UPDATE_DATE = SYSDATE
        WHERE CUSTOMER_ID = PIINCUSTOMER_ID;
        COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
    END;                                        
                                            
      POSTEXIT:= 'Registro Actualizado';    
  
  END UPDATE_CUSTOMER_PR; 
  
  PROCEDURE DELETE_CUSTOMER_PR (POSTERROR           OUT VARCHAR2,
                                POSTEXIT            OUT VARCHAR2,
                                PIINCUSTOMER_ID     IN NUMBER
                                )AS
  BEGIN
      BEGIN
          DELETE  CUSTOMER_TAB
          WHERE CUSTOMER_ID = PIINCUSTOMER_ID;
          COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
          POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;      
      END;
      POSTEXIT:= 'Registro Borrado';
  END DELETE_CUSTOMER_PR;                                
  
END CS_CUSTOMER_PKG;  
/