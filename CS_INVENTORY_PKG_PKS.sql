CREATE OR REPLACE PACKAGE CS_INVENTORY_PKG AS 
PROCEDURE INSERT_INVENTORY_PR (POSTERROR          OUT VARCHAR2,
                               POSTEXIT           OUT VARCHAR2,
                               PISTPRODUCT_NAME   IN VARCHAR2,
                               PISTDESCRIPTION    IN VARCHAR2,
                               PISTPRODUCT_TYPE   IN VARCHAR2,
							   PISTMEASURE        IN VARCHAR2,
                               PIINUNIT_PRICE     IN NUMBER
                              );


  PROCEDURE UPDATE_INVENTORY_PR (POSTERROR          OUT VARCHAR2,
                                 POSTEXIT           OUT VARCHAR2,
								 PIININV_ID         NUMBER,
                                 PISTPRODUCT_NAME   IN VARCHAR2,
                                 PISTDESCRIPTION    IN VARCHAR2,
                                 PISTPRODUCT_TYPE   IN VARCHAR2,
								 PISTMEASURE        IN VARCHAR2,
                                 PIINUNIT_PRICE     IN NUMBER
                                );


  PROCEDURE DELETE_INVENTORY_PR (POSTERROR      OUT VARCHAR2,
                                 POSTEXIT       OUT VARCHAR2,
                                 PIININV_ID     IN NUMBER
                                 );

END CS_INVENTORY_PKG;
/