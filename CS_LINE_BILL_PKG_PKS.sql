CREATE OR REPLACE PACKAGE CS_LINE_BILL_PKG AS

  PROCEDURE INSERT_LINES_PR (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PIINHEADER_ID      NUMBER,
                              PIININV_ID         NUMBER,
                              PIINAMOUNT         NUMBER    
                             );
                              
  
  PROCEDURE UPDATE_LINES_PR  (POSTERROR          OUT VARCHAR2,
                              POSTEXIT           OUT VARCHAR2,
                              PIINLINE_ID        NUMBER,
                              PIINHEADER_ID      NUMBER,
                              PIININV_ID         NUMBER,
                              PIINAMOUNT         NUMBER
                             );

  PROCEDURE DELETE_LINES_PR  (POSTERROR      OUT VARCHAR2,
                              POSTEXIT       OUT VARCHAR2,
                              PIINHEADER_ID  IN NUMBER,
                              PIINLINE_ID    IN NUMBER
                              );

END CS_LINE_BILL_PKG;    
/                          