CREATE OR REPLACE PACKAGE CS_CUSTOMER_PKG AS 
PROCEDURE INSERT_CUSTOMER_PR (POSTERROR           OUT VARCHAR2,
                              POSTEXIT            OUT VARCHAR2,
                              PISTCUSTOMER_NAME   IN VARCHAR2,
                              PISTCUST_LAST_NAME  IN VARCHAR2,
                              PISTCUSTOMER_CODE   IN VARCHAR2,
                              PISTCUSTOMER_CAT    IN VARCHAR2,
                              PISTADRESS          IN VARCHAR2,
                              PIINPHONE_NUMBER    IN NUMBER,
                              PISTEMAIL           IN VARCHAR2
                              );


PROCEDURE UPDATE_CUSTOMER_PR (POSTERROR           OUT VARCHAR2,
								POSTEXIT            OUT VARCHAR2,
								PIINCUSTOMER_ID     IN NUMBER,
								PISTCUSTOMER_NAME   IN VARCHAR2,
                                PISTCUST_LAST_NAME  IN VARCHAR2,
                                PISTCUSTOMER_CODE   IN VARCHAR2,
                                PISTCUSTOMER_CAT    IN VARCHAR2,
                                PISTADRESS          IN VARCHAR2,
                                PIINPHONE_NUMBER    IN NUMBER,
                                PISTEMAIL           IN VARCHAR2
							   );


  PROCEDURE DELETE_CUSTOMER_PR (POSTERROR           OUT VARCHAR2,
								POSTEXIT            OUT VARCHAR2,
								PIINCUSTOMER_ID     IN NUMBER
								);

END CS_CUSTOMER_PKG;
/