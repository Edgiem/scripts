CREATE OR REPLACE PACKAGE BODY CS_INVENTORY_PKG AS

PROCEDURE INSERT_INVENTORY_PR (POSTERROR          OUT VARCHAR2,
                               POSTEXIT           OUT VARCHAR2,
                               PISTPRODUCT_NAME   IN VARCHAR2,
                               PISTDESCRIPTION    IN VARCHAR2,
                               PISTPRODUCT_TYPE   IN VARCHAR2,
                               PISTMEASURE        IN VARCHAR2,
                               PIINUNIT_PRICE     IN NUMBER
                              ) IS
  
 linIdInv NUMBER;
  

  BEGIN
      SELECT INV_ID_SEQ.NEXTVAL INTO linIdInv FROM DUAL; 
      
      BEGIN
          INSERT INTO INVENTORY_TAB(INVENTORY_ID,
                                    PRODUCT_NAME,
                                    DESCRIPTION,
                                    PRODUCT_TYPE,
                                    MEASURE,
                                    UNIT_PRICE,
                                    CREATED_BY,
                                    CREATION_DATE,
                                    LAST_UPDATE_DATE)
                             VALUES(linIdInv,
                                    PISTPRODUCT_NAME,
                                    PISTDESCRIPTION,
                                    PISTPRODUCT_TYPE,
                                    PISTMEASURE,
                                    PIINUNIT_PRICE,
                                    '1',
                                    SYSDATE,
                                    SYSDATE
                                    ) ;
                                   COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
      END;                                        
                                            
      POSTEXIT:= 'Registro Creado';
   
  END INSERT_INVENTORY_PR;
  
  PROCEDURE UPDATE_INVENTORY_PR (POSTERROR          OUT VARCHAR2,
                                 POSTEXIT           OUT VARCHAR2,
                                 PIININV_ID         NUMBER,
                                 PISTPRODUCT_NAME   IN VARCHAR2,
                                 PISTDESCRIPTION    IN VARCHAR2,
                                 PISTPRODUCT_TYPE   IN VARCHAR2,
                                 PISTMEASURE        IN VARCHAR2,
                                 PIINUNIT_PRICE     IN NUMBER
                                ) AS
                               
                               
    lstName      VARCHAR2(300);  
    lstDesc      VARCHAR2(300);
    lstType      VARCHAR2(30);
    lstMeas      VARCHAR2(30);
    linPrice     NUMBER;                               
  
  BEGIN
    BEGIN
        SELECT PRODUCT_NAME,
               DESCRIPTION,
               PRODUCT_TYPE,
               MEASURE,
               UNIT_PRICE
        INTO  lstName,
              lstDesc,
              lstType,
              lstMeas,
              linPrice             
        FROM  INVENTORY_TAB                             
        WHERE INVENTORY_ID = PIININV_ID;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        POSTERROR:= ' No se encontro producto: '|| PIININV_ID ;
    END;    
                                
    BEGIN
        UPDATE INVENTORY_TAB SET PRODUCT_NAME   = NVL(PISTPRODUCT_NAME,lstName),
                                 DESCRIPTION    = NVL(PISTDESCRIPTION,lstDesc),
                                 PRODUCT_TYPE   = NVL(PISTPRODUCT_TYPE,lstType),
                                 MEASURE        = NVL(PISTMEASURE,lstMeas),
                                 UNIT_PRICE     = NVL(PIINUNIT_PRICE,linPrice)                                 
        WHERE INVENTORY_ID = PIININV_ID;
        COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;
    END;                                        
                                            
      POSTEXIT:= 'Registro Actualizado';    
  
  END UPDATE_INVENTORY_PR; 
  
  PROCEDURE DELETE_INVENTORY_PR (POSTERROR      OUT VARCHAR2,
                                 POSTEXIT       OUT VARCHAR2,
                                 PIININV_ID     IN NUMBER
                                 )AS
  BEGIN
      BEGIN
          DELETE  INVENTORY_TAB
          WHERE   INVENTORY_ID = PIININV_ID;
          COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
          POSTERROR:= 'Se ha producido el error - '||SQLCODE||' -ERROR- '||SQLERRM;      
      END;
      POSTEXIT:= 'Registro Borrado';
  END DELETE_INVENTORY_PR;                                
  
END CS_INVENTORY_PKG;  
/